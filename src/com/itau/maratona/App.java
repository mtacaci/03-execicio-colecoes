package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class App {
	public static void main(String[] args) {
		// String sep = System.getProperty("file.separator");
		// String caminho = System.getProperty("user.dir") + sep +"src" + sep +
		// "alunos.csv";
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> listaAluno = new ArrayList<>();
		List<Equipe> retornoEquipe = new ArrayList<>();
		listaAluno = arquivo.ler();
		retornoEquipe = new CriadorEquipe().listaEquipe(listaAluno, 3);
		Imprimir.imprimirEquipe(retornoEquipe);
	}
}
