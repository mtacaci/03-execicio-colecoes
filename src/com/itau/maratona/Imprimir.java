package com.itau.maratona;

import java.util.List;

public class Imprimir {
	public static void imprimirEquipe(List<Equipe> listaEquipe) {
		for (Equipe equipe : listaEquipe) {
			System.out.println("ID:" + equipe.id);
			for (String nome : equipe.listaEquipe) {
				System.out.println("Integrate: " + nome);
			}
			System.out.println("==================================");
		}
	}
}
