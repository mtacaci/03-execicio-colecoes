package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class CriadorEquipe {
	int controleEquipe = 1;
	int alunosEquipe = 0;
	int totalAluno = 0;
	int tamanhoEquipe = 0;
	int totalAlunoAtual = 0;
	List<Aluno> listaAluno = new ArrayList<>();

	public Equipe construir(int quantidade) {
		Equipe equipe = new Equipe();
		equipe.id = controleEquipe;
		for (int i = 0; i < quantidade; i++) {
			String nome = sortear();
			equipe.listaEquipe.add(nome);
		}
		controleEquipe++;
		return equipe;
	}

	public List<Equipe> listaEquipe(List<Aluno> listaAluno, int tamanhoEquipe) {
		this.listaAluno = listaAluno;
		totalAluno = listaAluno.size();
		totalAlunoAtual = totalAluno - 1;
		this.tamanhoEquipe = tamanhoEquipe;

		List<Equipe> retornoEquipe = new ArrayList<>();
		int interacoes = totalAluno / tamanhoEquipe;
		for (int i = 0; i < interacoes; i++) {
			retornoEquipe.add(construir(tamanhoEquipe));
		}
		return retornoEquipe;
	}

	private String sortear() {
		Double sorteio = (Math.random() * totalAlunoAtual);
		int sorteioInteiro = sorteio.intValue();
		Aluno aluno = listaAluno.get(sorteioInteiro);
		listaAluno.remove(aluno);
		totalAlunoAtual--;
		return aluno.nome;
	}
}
